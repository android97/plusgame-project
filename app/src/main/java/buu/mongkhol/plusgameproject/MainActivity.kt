package buu.mongkhol.plusgameproject

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        reGame()

    }

    private fun reGame() {
        val N1 = Random.nextInt(0, 11)
        val N2 = Random.nextInt(0, 11)
        val number1 = findViewById<TextView>(R.id.txtN1)
        val number2 = findViewById<TextView>(R.id.txtN2)

        val btn1 = findViewById<Button>(R.id.btn1)
        val btn2 = findViewById<Button>(R.id.btn2)
        val btn3 = findViewById<Button>(R.id.btn3)

        number1.text = "$N1"
        number2.text = "$N2"

        val answer = N1 + N2
        val answer2 = answer.toString()

        randomAnswer(btn1, btn2, btn3, answer)
        check(btn1, btn2, btn3, answer2)

    }

    private fun randomAnswer(
        btn1: Button,
        btn2: Button,
        btn3: Button,
        answer: Int
    ) {
        val ans: Int = Random.nextInt(0, 2)
        if (ans == 0) {
            btn1.text = (answer + 0).toString()
            btn2.text = (answer + 5).toString()
            btn3.text = (answer + 2).toString()
        }
        if (ans == 1) {
            btn1.text = (answer - 1).toString()
            btn2.text = (answer + 0).toString()
            btn3.text = (answer + 2).toString()
        }
        if (ans == 2) {
            btn1.text = (answer - 2).toString()
            btn2.text = (answer + 3).toString()
            btn3.text = (answer + 0).toString()
        }
    }

    private fun check(
        btn1: Button,
        btn2: Button,
        btn3: Button,
        answer2: String

    ){
        val txtTr = findViewById<TextView>(R.id.txtTr)
        val txtFa = findViewById<TextView>(R.id.txtFa)
        val txtTrue = findViewById<TextView>(R.id.txtTrue)
        val txtFalse = findViewById<TextView>(R.id.txtFalse)
        val txtAns = findViewById<TextView>(R.id.txtAns)

            btn1.setOnClickListener {
                if (answer2 == btn1.text) {
                    Toast.makeText(this, "ถูกต้อง", Toast.LENGTH_SHORT).show()
                    txtAns.text = "ถูกต้อง".toString()
                    txtTr.text = (txtTr.text.toString().toInt() + 1).toString()
                    txtTrue.text = "ถูก : "
                    btn1.setBackgroundColor(Color.parseColor("#99ff99"))
                    reGame()
                } else {
                    Toast.makeText(this, "ไม่ถูกต้อง", Toast.LENGTH_SHORT).show()
                    txtAns.text = "ไม่ถูกต้อง".toString()
                    txtFa.text = (txtFa.text.toString().toInt() + 1).toString()
                    txtFalse.text = "ผิด : "
                    btn1.setBackgroundColor(Color.parseColor("#ff6633"))
                    reGame()
                }
            }
            btn2.setOnClickListener {
                if (answer2 == btn2.text) {
                    Toast.makeText(this, "ถูกต้อง", Toast.LENGTH_SHORT).show()
                    txtAns.text = "ถูกต้อง".toString()
                    txtTr.text = (txtTr.text.toString().toInt() + 1).toString()
                    txtTrue.text = "ถูก : "
                    btn2.setBackgroundColor(Color.parseColor("#99ff99"))
                    reGame()
                } else {
                    Toast.makeText(this, "ไม่ถูกต้อง", Toast.LENGTH_SHORT).show()
                    txtAns.text = "ไม่ถูกต้อง".toString()
                    txtFa.text = (txtFa.text.toString().toInt() + 1).toString()
                    txtFalse.text = "ผิด : "
                    btn2.setBackgroundColor(Color.parseColor("#ff6633"))
                    reGame()
                }
            }
            btn3.setOnClickListener {
                if (answer2 == btn3.text) {
                    Toast.makeText(this, "ถูกต้อง", Toast.LENGTH_SHORT).show()
                    txtAns.text = "ถูกต้อง".toString()
                    txtTr.text = (txtTr.text.toString().toInt() + 1).toString()
                    txtTrue.text = "ถูก : "
                    btn3.setBackgroundColor(Color.parseColor("#99ff99"))
                    reGame()
                } else {
                    Toast.makeText(this, "ไม่ถูกต้อง", Toast.LENGTH_SHORT).show()
                    txtAns.text = "ไม่ถูกต้อง".toString()
                    txtFa.text = (txtFa.text.toString().toInt() + 1).toString()
                    txtFalse.text = "ผิด : "
                    btn3.setBackgroundColor(Color.parseColor("#ff6633"))
                    reGame()
                }
            }

    }
}